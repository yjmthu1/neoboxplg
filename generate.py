import json, os, platform, shutil, zipfile, tarfile, sys

plat = platform.system().lower()

prefixs = {
  'windows': '',
  'linux': 'lib'
}

suffixs = {
  'windows': 'dll',
  'linux': 'so'
}

compress = {
  'windows': 'zip',
  'linux': 'tar.gz'
}

def walkdir(subdir):
  js = dict()
  for root, dirs, files in os.walk(subdir, True):
    if "manifest.json" in files:
      files.remove("manifest.json")
    js[root] = dict()
    js[root]["dirs"] = dirs
    js[root]["files"] = files
  with open(os.path.join(subdir, "manifest.json"), 'w', encoding='utf8') as fp:
    json.dump(js, fp, ensure_ascii=False)

def zipdir(path, ziph):
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):
    for file in files:
      ziph.write(os.path.join(root, file))

def copyplg(d: str):
  filename = f'{prefixs[plat]}{d}.{suffixs[plat]}'
  src = os.path.join('..', '..', 'neobox-plugins', 'install', filename)
  src = os.path.abspath(src)
  dst = os.path.join(d, filename)
  print(f'copy <{src}> to <{dst}>.')
  shutil.copyfile(src, dst)

if __name__ == '__main__':
  os.chdir(plat)
  arglist = sys.argv[1:]
  if len(arglist) == 0:
    arglist = os.listdir('.')
  for subdir in arglist:
    if not os.path.isdir(subdir):
      continue
    copyplg(subdir)
    os.chdir(subdir)
    compressed = f'../{subdir}.{compress[plat]}'
    if plat == 'windows':
      zip_file = zipfile.ZipFile(compressed, 'w', zipfile.ZIP_DEFLATED)
      zipdir('.', zip_file)
      zip_file.close()
    else:
      with tarfile.open(compressed, "w:gz") as tar:
        tar.add('.', arcname=os.path.basename('.'))
    os.chdir('..')
    # walkdir(d)
